from setuptools import setup

setup(
    name='ATS',
    version='1.7',
    author='Mark Tymoshenko',
    author_email='mark@tymoshenko.xyz',
    description='Description of your package',
    packages=['your_package_name'],  # List of packages to be included in the distribution
    install_requires=[
        # List your project dependencies here
        'dependency1',
        'dependency2',
    ],
)