# Copyright (c) 2023 SafeCom-Wireless
# Authors:
# Mark Tymoshenko mark@tymoshenko.xyz
# Ulric Lewis 



import csv
import time
import ctypes
import os
import numpy as np
import tkinter as tk
from matplotlib.figure import Figure 
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk) 
from tkinter import *
from tkinter import ttk
from tkinter import font
from tkinter import filedialog, messagebox
from ctypes import *
from decimal import *
from itertools import islice
import threading
import datetime

# Import Signal Hound API functions and constants (replace 'sg_api' with the actual module name)
from sg_api import *

# Import Spectrum Analyzer API functions and constants (replace 'sa_api' with the actual module name)
from sa_api import *

# ... (Signal Houndd and Spectrum Analyzer API functions and constants go here)

# index's into test list
#
SELECT_PARAMETER =0
FREQUENCY_PARAMETER =1
INPUT_PARAMETER =2
OUTPUTMIN_PARAMETER =3
OUTPUTMAX_PARAMETER =4

# globals here

version_text ="RF Output Power Measurement V1.9  (Internal Use Only Do Not Distribute)"

script_lines =[];				# script line buffer
sa_device_handle =-1;
sg_device_handle =-1;
sweepLen = c_int(-1);
startFreq = c_double(-1);
binSize = c_double(-1);
outputValue ="";
stop_event = threading.Event()
selectMode ="";

#time functions for terminal log
def get_current_time():
    return datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")

#text colors
RED = '\033[91m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RESET = '\033[0m'
# Function to start measurements, execute each line in tree view
#
def start_measurement():
    
    global selectedItem	    								# update global
    
    if demo_state.get ==0:								    # demo off - connect
        signal_connect()    								# Check if Signal Hound device is connected
        if signal_connected_state.get() ==0:
            return

        analyzer_connect()   								# Check if Spectrum Analyzer device is connected
        if analyzer_connected_state.get() ==0:
            return

    test_table.selection_remove(*test_table.selection())                                            # deselect all
    print(GREEN+'Test Started                           ',get_current_time()+RESET)
    for selectedItem in test_table.get_children():					                                # run test
        test_table.selection_set(selectedItem)						                                # highlight line we are on
        test_table.focus(selectedItem)							                                    # focus on it to read data
 
        if test_table.item(selectedItem)['values'][SELECT_PARAMETER] =="  ":                        # line selected?
            continue                                                                                # no, next

        signal_frequency_var.set(test_table.item(selectedItem)['values'][FREQUENCY_PARAMETER])		# setup SG
							                                        # update display

        # if squelch test, bump input 1db until squelch opens outputmin, then twice more to get outputmax
        #
        if test_table.item(selectedItem)['values'][SELECT_PARAMETER] =="SQ":                        # squelch test?

            vinput =int(test_table.item(selectedItem)['values'][INPUT_PARAMETER])                   # get start level

            for level in range (100):                                                               # 100 steps!

                if stop_event.is_set():                                                             # stop test?
                    return                                                                          # bye!
 
                signal_gain_var.set(str(vinput))                                                    # set input signal
                signal_frame.update()						                                        # update display
                signal_program()					                                                # program/sweep 
                vinput +=1                                                                          # increase signal 1db
                test_table.set(selectedItem, 'outputmin', outputValue)					            # update output
                
            continue                                                                                # next selected line
        
        # if here normal INPUT signal handling
        #
        signal_gain_var.set(test_table.item(selectedItem)['values'][INPUT_PARAMETER])
        signal_frame.update()	
        signal_program()								                                            # program/sweep
        test_table.set(selectedItem, 'outputmin', outputValue)							            # update output

        								                                                            # analyze
        delay =flow_delay_var.get()							                                        # get delay ms
        if delay:									                                                # if non-zero, delay ms
            print(GREEN+'Waiting:',delay, 'ms                        ',get_current_time()+RESET)
            time.sleep(delay/1000)  # 0.8)								                                            # delay secs

        test_table.selection_remove(selectedItem)
                        # done, deselect
        if stop_event.is_set():
            break
        else:
            continue                
    if stop_event.is_set():
        print(RED+"Test stopped                           ",get_current_time()+RESET)
    else:
        print(GREEN+"Test Complete!!!                       ",get_current_time()+RESET)
        

# stop
def stop_measurement():
    stop_event.set()
       

# pause
def start_measurement_thread():

    global measurement_thread
    
    stop_event.clear()
    measurement_thread = threading.Thread(target=start_measurement)
    measurement_thread.start()

# Function to stop the measurement thread
def stop_measurement_thread():
    global measurement_thread

    stop_event.set()

def reset_measurement():
    print(YELLOW+"Test Reset and Cleared                 ", get_current_time()+RESET)
    clear_test()
 
# run config file
def run_config():

    config_file_path = filedialog.askopenfilename(title="Select Configuration File", filetypes=[("Config files", "*.txt")])
    if config_file_path:
        with open(configt_file_path, newline='') as f:				        # open file
            lines = f.readlines()						                    # read in all lines
            input_text.delete(1.0, tk.END)  					            # Clear previous content
            for line in lines:							                    # scan through lines
                input_text.insert(tk.END, line)					            # update screen
                line =line.strip()						                    # strip spaces
                if line:							                        # if ine not empty
                    data =[item.strip() for item in line.split(' ')]		# seperate items by ' '
                    for index, elem in enumerate(data):				        # for number found
                        if data[index] and data[index] != '#' and data[index] != ' ':	# only save real data
                            print(data[index])
                            script_lines.append(data[index])			    # into script line buffer
                        else:                                               # else ignore noise
                            continue

        f.close()								                            # tidy up

        for index, elem in enumerate(script_lines):				            # now process config commands
            if script_lines[index] =="set":
                print("found set")


# signal connect
def signal_connect():

    global sg_device_handle

    if demo_state.get() ==1:							# demo, ignore
        return

    if signal_connected_state.get() ==1:
        return

    device = sg_open_device()
    if device["status"] != 0:
        messagebox.showerror(RED+"Error", "Signal Generator not connected.")
        signal_connected_state.set(0)
    else:
        signal_connected_state.set(1)
        print(GREEN+'Signal Generator connected             ', get_current_time()+RESET)
    sg_device_handle =device
 
# signal program
#
def signal_program():

    global sg_device_handle

    if demo_state.get() ==0:								# non-demo - connect
        if signal_connected_state.get() ==0:
            signal_connect()

        device =sg_device_handle["handle"]
        print(GREEN+'Testing at:',int(float(signal_frequency_var.get())*1e6),'MHz',float(signal_gain_var.get()),'dBm    ', get_current_time()+RESET)

        sg_set_frequency_amplitude(device, int(float(signal_frequency_var.get())*1e6), float(signal_gain_var.get()))

        #if signal_mode_var =="FM":
         #   sg_set_fm(device, 25.0e3, 4.0e2, SG_SHAPE_SINE) 

        #if signal_mode_var =="CW":
        sg_set_cw(device) 

        #    if signal_mode_var =="AM":
        #        sg_set_am(device, frequency, depth, shape)
        
    if selectedItem:									# user selected?
        test_table.set(selectedItem, 'frequency', signal_frequency_var.get())
        test_table.set(selectedItem, 'input', signal_gain_var.get())
        test_table.focus(selectedItem)        
 
    if flow_track_state.get() ==1:							# analyzer tracks signal ?
        analyzer_center_var.set(signal_frequency_var.get())				# copy Freq
        analyzer_frame.update();							# update screen
        analyzer_program()								# update analyzer

    analyzer_sweep()    								# now update sweep
    if demo_state.get() ==0:                            # non-demo
        sgRFOff(device)							        # turn off rf after ever mesuremnt


# add signal to list
#
def signal_add():

    global selectedItem									# update global

    selectedItem =test_table.insert('', 0)

    test_table.focus(selectedItem)
    signal_program()


# analyzer connect
def analyzer_connect():

    global sa_device_handle

    if demo_state.get() ==1:                                # demo mode - ignore
        return

    if analyzer_connected_state.get() ==1:
        return

    device = sa_open_device()
    if device["status"] != 0:
        messagebox.showerror(RED+"Error", "Spectrum Analyzer device not connected.")
        # Close Signal Hound device
        sg_close_device(sa_device_handle["handle"])
        analyzer_connected_state.set(0)
    else:
        analyzer_connected_state.set(1)
        print(GREEN+'Spectrum Analyzer connected            ',get_current_time()+RESET)

    sa_device_handle =device


# analyzer program
def analyzer_program():
    
    global sa_device_handle

    if demo_state.get() ==1:
        return

    if analyzer_connected_state.get() ==0:					# must be connected!
        analyzer_connect()


    device =sa_device_handle["handle"]
    #print(int(float(analyzer_center_var.get())*1e6) ,1.0e6*2.0)
    sa_config_center_span(device, float(analyzer_center_var.get())*1e6 , float(analyzer_span_var.get()))
    sa_config_level(device, float(analyzer_level_var.get()))
    sa_config_acquisition(device, SA_MIN_MAX, SA_LOG_SCALE)
    sa_config_sweep_coupling(device, float(analyzer_sweep_var.get()), float(analyzer_coupling_var.get()), 1)

#    sa_config_gain_atten(device, float(analyzer_atten_var.get()), float(analyzer_gain_var.get()), 0)


#analyzer sweep
def analyzer_sweep():

    global sweepLen
    global startFreq
    global binSize
    global sa_device_handle

    if demo_state.get() ==1:							# demo
        if selectedItem:							# line selected?
            test_table.set(selectedItem, 'outputmin', "Min")
            test_table.set(selectedItem, 'outputmax', "Max")

        return

    if analyzer_connected_state.get() ==0:					# must be connected!
        analyzer_connect()

    device =sa_device_handle["handle"]

    sa_config_center_span(device, float(analyzer_center_var.get())*1.0e6 , float(analyzer_span_var.get()))
    sa_config_acquisition(device, SA_MIN_MAX, SA_LOG_SCALE)
    sa_config_level(device, -float(analyzer_level_var.get()))
    sa_config_sweep_coupling(device, float(analyzer_sweep_var.get()), float(analyzer_coupling_var.get()), 1)

    sa_initiate(device, SA_SWEEPING, 0)    					# Initialize the device with the configuration just set

    sweepInfo =sa_query_sweep_info(device)
    sweepLen = sweepInfo["sweep_length"]
    startFreq = sweepInfo["start_freq"]
    binSize = sweepInfo["bin_size"]
 
    getSweep =sa_get_sweep_32f(device)
    min =getSweep["min"]
    max =getSweep["max"]

    analyzer_plot(max)        							# display sweep
 
    saAbort(device)

# plot analyzer data
#
def analyzer_plot(max):

    global sweepLen
    global startFreq
    global binSize
    global selectedItem
    global outputValue

    if demo_state.get() ==1:									# demo - ignore
        return

    if analyzer_connected_state.get() ==0:						# must be connected!
        analyzer_connect()
  
    fig = Figure(figsize = (12, 6), dpi = 50)     				# plot area
 
    plot1 = fig.add_subplot(111) 								# adding the subplot
    plot1.plot(max)										        # plot MAX values

    mm = -200											        # find max level
    xx =0
    for i in range(sweepLen):									# for entire sweep
        if max[i] > mm:										    # highest level
            mm =max[i]
            xx =i

    s =str(max[xx]+ 10)										    # string
    plot1.text(xx, mm, s )								        # write level at point	
    outputValue =s                                              # save output value
    plot1.grid(color = 'green', linestyle = '--', linewidth = 0.5)				# draw grid

    canvas = FigureCanvasTkAgg(fig, master = output_frame)      # create output window
    canvas.draw() 
    canvas.get_tk_widget().grid(row=0, column=0)


# test line(s) selected
#
def test_selected(a):

    selects =test_table.selection()                                                                             # get selected tuple

    for selthis in selects:    
        test_table.selection_remove(selthis)                                                                    # remove selection


# select test mode with space bar
def test_mode(a):

    global selectedItem										                                                    # update global's
    global selectMode

    selects =test_table.selection()                                                                             # get selected tuple
    num =len(selects)                                                                                           # get how many selected
    selnum =0                                                                                                   # select count
    for selthis in selects:                                                                                     # scan through list

        if selnum ==0:                                                                                          # first line
            if test_table.item(selthis)['values'][0] =="IN":                                                    # input
                test_table.set(selthis, 'run', "SQ")
            elif test_table.item(selthis)['values'][0] =="SQ":                                                  # squelch
                test_table.set(selthis, 'run', "  ")							                                # update output
            else:
                test_table.set(selthis, 'run', "IN")

            selectMode =test_table.item(selthis)['values'][0]                                                   # save mode
        else:
            test_table.set(selthis, 'run', selectMode )                                                         # force all same

        #test_table.selection_remove(selthis)                                                                    # remove selection
        selnum +=1

# set start test line 
#
def test_execute():
    global selectedItem

    test_table.selection_remove(selectedItem)


# open input CSV file
#
def OpenInput():
    global selectedItem

    input_file_path = filedialog.askopenfilename(title="Load Input", filetypes=[("Excel files", "*.xlsx")])     # get file
    
    s =os.path.split(input_file_path)[1]							                                            # get just file name
    print(s.split('.')[0])
    f =s.split('.')                                                                                             # split file.slsx [0][1]
    output_file_save_name.set(f[0])								                                                # set to autosave file

    with open(input_file_path, 'r', newline='') as csvFile: # open it
        test_data = csv.reader(csvFile, delimiter=',', quoting=csv.QUOTE_NONE)                                  # read in entire file
        # Skip the first 2 rows of the file
        skipped_data = islice(test_data, 1, None)
        
        test_table.delete(*test_table.get_children()) # clear table
        for frequency, gain in skipped_data: # save test data to tree view
            selectedItem = test_table.insert('', tk.END) # insert new line at end
            test_table.set(selectedItem, 'run', "IN")                                                           # default INput
            test_table.set(selectedItem, 'frequency', frequency)                                                # fill in freq/gain
            test_table.set(selectedItem, 'input', gain)
        
        selectedItem = test_table.get_children()[0] 						                                    # first line
        test_table.selection_set(selectedItem)							                                        # highlight line we are on
        test_table.focus(selectedItem)
        
    csvFile.close()
    print(GREEN+'Input file loaded: ',input_file_path+RESET)										            # tidy up

# save input CSV file
#
def SaveInput():

    csvNames = ['frequency', 'input']

    output_file_path = filedialog.asksaveasfilename(title="Save Input", filetypes=[("Excel files", "*.xlsx")])

    with open(output_file_path, 'w') as csvFile: 

       csvWrite = csv.DictWriter(csvFile, fieldnames=csvNames)
       csvWrite.writerow({'frequency': 'Frequency (hz)', 'input': ' Input Level (dbm)'})
       for point in test_table.get_children():
           # write table line with results [2]
           csvWrite.writerow({'frequency': test_table.item(point)['values'][FREQUENCY_PARAMETER], 'input': test_table.item(point)['values'][INPUT_PARAMETER]})
   
    csvFile.close()
    print(GREEN+'Input file saved'+RESET)

# save output CSV file
#
def SaveOutput():

    csvNames = ['frequency', 'input', 'outputmin', 'outputmax']

    fn =""											                                        # default no initial file name
    if output_file_save_state.get() ==1:							                        # autosave mode?
        fn =output_file_save_name.get() + "-" + output_file_save_number.get() + ".xlsx"		# generate initial file name-xxxx.csv
           
    output_file_path = filedialog.asksaveasfilename(title="Save Output", initialfile=fn, filetypes=[("Excel files", "*.xlsx")])

    with open(output_file_path, 'w') as csvFile: 

       csvWrite = csv.DictWriter(csvFile, fieldnames=csvNames)
       csvWrite.writerow({'frequency': 'Frequency (hz)', 'input': ' Input Level (dbm)', 'outputmin':' Output Level Min (dbm)', 'outputmax':' Output Level Max (dbm)'})
       for point in test_table.get_children():
           # write table line with results [2]
           csvWrite.writerow({'frequency': test_table.item(point)['values'][FREQUENCY_PARAMETER], 'input': test_table.item(point)['values'][INPUT_PARAMETER], 'outputmin': test_table.item(point)['values'][OUTPUTMIN_PARAMETER], 'outputmax': test_table.item(point)['values'][OUTPUTMAX_PARAMETER]})
   
    csvFile.close()
    if output_file_save_state.get() ==1:							                        # autosave mode?
        if output_file_inc_state.get() ==1:							                        # auto-inc?
            n =int(output_file_save_number.get())+1						                    # last +1
            output_file_save_number.set(str(n).zfill(4))					                # save as xxxx

# view output file
#
def ViewOutput():

    global selectedItem

    input_file_path = filedialog.askopenfilename(title="View Output", filetypes=[("Excel files", "*.xlsx")])	# get file

    with open(input_file_path, 'r', newline='') as csvFile:					                # open it

        test_data = csv.reader(csvFile, delimiter=',', quoting=csv.QUOTE_NONE)			    # read in entire file

        test_table.delete(*test_table.get_children())						                # clear table

        for frequency, gain, outputmin, outputmax in test_data:             	            # save test data to tree view
            selectedItem =test_table.insert('', tk.END)						                # insert new line at end
            test_table.set(selectedItem, 'frequency', frequency)				            # fill in freq/gain/output
            test_table.set(selectedItem, 'input', gain)
            test_table.set(selectedItem, 'outputmin', outputmin) 
            test_table.set(selectedItem, 'outputmax', outputmax) 

        selectedItem = test_table.get_children()[0] 						                # first line
        test_table.selection_set(selectedItem)							                    # highlight line we are on
        test_table.focus(selectedItem)

        csvFile.close()										                                # tidy up

# clear test table
#
def clear_test():
    test_table.delete(*test_table.get_children())						# clear table
    test_frame.update()

# select demo mode
#
def demoMode():
    if demo_state.get() ==1:
        demo_state.set(0)
        root.title(version_text)
    else:
        demo_state.set(1)
        root.title(version_text + "Demo Mode")
    root.update()

# do nothing - debug
def donothing():
    print("nothing")

#thread = threading.Thread(target=start_measurement)
#thread.start()

#===================================================================================================
#
# huston, we have liftoff
#
root = tk.Tk()

selectedItem =IntVar();

lcdFontSmall = font.Font(family="LCD", size=10, weight=font.BOLD) 
lcdFontLarge = font.Font(family="LCD", size=20, weight=font.BOLD) 

root.geometry('940x800') 
root['padx'] = 5
root['pady'] = 5

root.title(version_text)

photo = tk.PhotoImage(file = 'safecom.png')
root.wm_iconphoto(False, photo)


# test table

test_frame = ttk.LabelFrame(root, text="Input", relief=tk.RIDGE)
test_frame.grid(row=1, column=1, sticky=tk.E + tk.W + tk.N + tk.S, pady=5)

test_columns = ('run', 'frequency', 'input', 'outputmin', 'outputmax')

test_table = ttk.Treeview(test_frame, columns=test_columns, show='headings')
test_table.grid(row=1, column=1, sticky=tk.W)

test_table.heading('run', text='Run')
test_table.heading('frequency', text='Frequency MHz')
test_table.heading('input', text='Input dbm')
test_table.heading('outputmin', text='Output Min dbm')
test_table.heading('outputmax', text='Output Max dbm')

test_table.column('#1', stretch='no', minwidth=0, width=40)
test_table.column('#2', stretch='no', minwidth=0, width=150)
test_table.column('#3', stretch='no', minwidth=0, width=150)
test_table.column('#4', stretch='no', minwidth=0, width=150)
test_table.column('#5', stretch='no', minwidth=0, width=150)

#test_table.bind('<ButtonRelease-1>', test_selected)               # left button select/deselect line(s)
test_table.bind('<space>', test_mode)                             # space bar to change test mode

test_scrollbar = ttk.Scrollbar(test_frame, orient=tk.VERTICAL, command=test_table.yview)
test_table.configure(yscroll=test_scrollbar.set)
test_scrollbar.grid(row=1, column=2, sticky=tk.W)


# spectrum analyzer output frame
output_frame = ttk.LabelFrame(root, text="Output", relief=tk.RIDGE)
output_frame.grid(row=2, column=1, sticky=tk.E + tk.W + tk.N + tk.S, pady=5)
#output_text = tk.Text(output_frame, height=15, width=75)
#output_text.grid(row=2, column=1, sticky=tk.W, pady=5)


# signal generator frame
signal_frequency_var=StringVar()
signal_gain_var=StringVar()
signal_mode_var=StringVar()
signal_pulse_var=StringVar()
signal_sweep_var=StringVar()

signal_frequency_var.set("200.0000")
signal_gain_var.set("1.0")

signal_frame = ttk.LabelFrame(root, width=375, height=115, text="Signal Generator", relief=tk.RIDGE)
signal_frame.grid(row=1, column=2, sticky=tk.E + tk.W + tk.N + tk.S, padx=5, pady=5)

signal_frequency_value = ttk.LabelFrame(signal_frame, text="Frequency MHz", relief=tk.RIDGE)
signal_frequency_value.grid(row=1, column=1, columnspan=2, sticky=tk.E + tk.W + tk.N + tk.S)

signal_frequency_entry = ttk.Entry(signal_frequency_value, textvariable=signal_frequency_var, width=10, foreground="orange", font=lcdFontLarge)
signal_frequency_entry.grid(row=1, column=1,  sticky=tk.W, pady=3)

signal_connected_state =IntVar()
signal_connected_state.set(0)
signal_connected_check = ttk.Checkbutton(signal_frequency_value, variable=signal_connected_state, onvalue=1, offvalue=0, text="Connected")
signal_connected_check.grid(row=1, column=2)


signal_control_frame = ttk.LabelFrame(signal_frame, text="Control", relief=tk.RIDGE)
signal_control_frame.grid(row=2, column=1, columnspan=2, sticky=tk.E + tk.W + tk.N + tk.S, pady=5)

signal_gain_label = ttk.Label(signal_control_frame, text="Gain")
signal_gain_label.grid(row=1, column=1, sticky=tk.W + tk.N)
signal_gain_entry = ttk.Entry(signal_control_frame, textvariable=signal_gain_var, width=10, font=lcdFontSmall)
signal_gain_entry.grid(row=1, column=2, sticky=tk.W, pady=3)
'''
signal_mode_label = ttk.Label(signal_control_frame, text="Mode")
signal_mode_label.grid(row=2, column=1, sticky=tk.W + tk.N)
signal_mode_combobox = ttk.Combobox(signal_control_frame, height=3, font=lcdFontSmall, textvariable=signal_mode_var)
signal_mode_combobox.grid(row=2, column=2, sticky=tk.W, pady=3)
signal_mode_combobox['values'] = ("CW", "FM")
signal_mode_combobox.current(0)			# default CW
'''
signal_pulse_label = ttk.Label(signal_control_frame, text="Pulse")
signal_pulse_label.grid(row=3, column=1, sticky=tk.W + tk.N)
signal_pulse_entry = ttk.Entry(signal_control_frame, width=10, font=lcdFontSmall, textvariable=signal_pulse_var)
signal_pulse_entry.grid(row=3, column=2, sticky=tk.W, pady=3)

signal_sweep_label = ttk.Label(signal_control_frame, text="Sweep")
signal_sweep_label.grid(row=4, column=1, sticky=tk.W + tk.N)
signal_sweep_entry = ttk.Entry(signal_control_frame, width=10, font=lcdFontSmall, textvariable=signal_sweep_var)
signal_sweep_entry.grid(row=4, column=2, sticky=tk.W, pady=3)

signal_program_button = tk.Button(signal_control_frame, width=10, pady=3, bg='blue', fg='black', text="Program", command=signal_program)
signal_program_button.grid(row=5, column=1, columnspan=1, sticky=tk.W, pady=5)

signal_add_button = tk.Button(signal_control_frame, width=10, pady=3, bg='orange', fg='black', text="Add", command=signal_add)
signal_add_button.grid(row=5, column=2, columnspan=1, sticky=tk.W, pady=5)

"""
sgSetFrequencyAmplitude
sgRFOff
sgSetCW
sgSetAM
sgSetFM
sgSetPulse
sgSetSweep
sgSetMultitone
"""


# analyzer 
analyzer_center_var=tk.StringVar()
analyzer_span_var=tk.StringVar()
analyzer_level_var=tk.StringVar()
analyzer_gain_var=tk.StringVar()
analyzer_atten_var=tk.StringVar()
analyzer_sweep_var=tk.StringVar()
analyzer_coupling_var=tk.StringVar()
analyzer_units_var=tk.IntVar()

analyzer_center_var.set("200.0000")
analyzer_level_var.set("10.0")
analyzer_span_var.set("1.0E6")
analyzer_sweep_var.set("1.0E3")
analyzer_coupling_var.set("1.0E3")

analyzer_frame = ttk.LabelFrame(root, text="Spectrum Analyzer", relief=tk.RIDGE, padding=6)
analyzer_frame.grid(row=2, column=2, padx=5, pady=5, sticky=tk.E + tk.W + tk.N + tk.S)

analyzer_center_frame = ttk.LabelFrame(analyzer_frame, text="Center Frequency Mhz", relief=tk.RIDGE)
analyzer_center_frame.grid(row=1, column=1, columnspan=2, sticky=tk.E + tk.W + tk.N + tk.S, pady=5)

analyzer_center_entry = ttk.Entry(analyzer_center_frame, textvariable=analyzer_center_var, width=10, foreground="orange", font=lcdFontLarge)
analyzer_center_entry.grid(row=1, column=1, sticky=tk.W, pady=3)

analyzer_connected_state =IntVar()
analyzer_connected_state.set(0)
analyzer_connected_check = ttk.Checkbutton(analyzer_center_frame, variable=analyzer_connected_state, onvalue=1, offvalue=0, text="Connected")
analyzer_connected_check.grid(row=1, column=2, sticky=tk.W, pady=3)

"""
saConfigAcquisition
saConfigCenterSpan
saConfigLevel
saConfigGainAtten
saConfigSweepCoupling
saConfigRBWShape
saConfigProcUnits
saConfigIQ
saConfigAudio
saConfigRealTime
saConfigRealTimeOverlap
"""

analyzer_control_frame = ttk.LabelFrame(analyzer_frame, text="Control", relief=tk.RIDGE)
analyzer_control_frame.grid(row=2, column=1, columnspan=2, sticky=tk.E + tk.W + tk.N + tk.S, pady=5)

analyzer_span_label = ttk.Label(analyzer_control_frame, text="Span")
analyzer_span_label.grid(row=1, column=1, sticky=tk.W + tk.N)
analyzer_span_entry = ttk.Entry(analyzer_control_frame, textvariable=analyzer_span_var, width=10, font=lcdFontSmall)
analyzer_span_entry.grid(row=1, column=2, sticky=tk.W, pady=3)

analyzer_level_label = ttk.Label(analyzer_control_frame, text="Level")
analyzer_level_label.grid(row=2, column=1, sticky=tk.W + tk.N)
analyzer_level_entry = ttk.Entry(analyzer_control_frame, textvariable=analyzer_level_var, width=10, font=lcdFontSmall)
analyzer_level_entry.grid(row=2, column=2, sticky=tk.W, pady=3)

analyzer_gain_label = ttk.Label(analyzer_control_frame, text="Gain")
analyzer_gain_label.grid(row=3, column=1, sticky=tk.W + tk.N)
analyzer_gain_entry = ttk.Entry(analyzer_control_frame, textvariable=analyzer_gain_var, width=10, font=lcdFontSmall)
analyzer_gain_entry.grid(row=3, column=2, sticky=tk.W, pady=3)

analyzer_atten_label = ttk.Label(analyzer_control_frame, text="Attenuation")
analyzer_atten_label.grid(row=4, column=1, sticky=tk.W + tk.N)
analyzer_atten_entry = ttk.Entry(analyzer_control_frame, textvariable=analyzer_atten_var, width=10, font=lcdFontSmall)
analyzer_atten_entry.grid(row=4, column=2, sticky=tk.W, pady=3)

analyzer_sweep_label = ttk.Label(analyzer_control_frame, text="Sweep")
analyzer_sweep_label.grid(row=5, column=1, sticky=tk.W + tk.N)
analyzer_sweep_entry = ttk.Entry(analyzer_control_frame, textvariable=analyzer_sweep_var, width=10, font=lcdFontSmall)
analyzer_sweep_entry.grid(row=5, column=2, sticky=tk.W, pady=3)

analyzer_coupling_label = ttk.Label(analyzer_control_frame, text="Coupling")
analyzer_coupling_label.grid(row=6, column=1, sticky=tk.W + tk.N)
analyzer_coupling_entry = ttk.Entry(analyzer_control_frame, textvariable=analyzer_coupling_var, width=10, font=lcdFontSmall)
analyzer_coupling_entry.grid(row=6, column=2, sticky=tk.W, pady=3)

analyzer_unit_label = ttk.Label(analyzer_control_frame, text="Unit")
analyzer_unit_label.grid(row=7, column=1, sticky=tk.W + tk.N)
analyzer_unit_combobox = ttk.Combobox(analyzer_control_frame, height=3, font=lcdFontSmall, textvariable=analyzer_units_var)
analyzer_unit_combobox.grid(row=7, column=2, sticky=tk.W, pady=3)
analyzer_unit_combobox['values'] = ("DBM", "MV", "MW")
analyzer_unit_combobox.current(0)			# default dbm

analyzer_program_button = tk.Button(analyzer_control_frame, width=10, pady=3, bg='blue', fg='black', text="Program", command=analyzer_program)
analyzer_program_button.grid(row=8, column=1, sticky=tk.W, pady=5)
analyzer_sweep_button = tk.Button(analyzer_control_frame, width=10, pady=3, bg='orange', fg='black', text="Sweep", command=analyzer_sweep)
analyzer_sweep_button.grid(row=8, column=2, sticky=tk.W, pady=5)

# runtime frame
run_frame = ttk.LabelFrame(root, text="Test", relief=tk.RIDGE, padding=6)
run_frame.grid(row=3, column=1, columnspan=2, padx=6, sticky=tk.E + tk.W + tk.N + tk.S)

# control buttons
run_control_frame = ttk.LabelFrame(run_frame, text="Control", relief=tk.RIDGE, padding=6)
run_control_frame.grid(row=1, column=1, padx=6, sticky=tk.E + tk.W + tk.N + tk.S)

start_button = tk.Button(run_control_frame, width=10, bg='green', fg='black', text="Run", command=start_measurement_thread)
stop_button = tk.Button(run_control_frame, width=10, bg='red', fg='black', text="Stop", command=stop_measurement_thread)
pause_button = tk.Button(run_control_frame, width=10, bg='orange', fg='black', text="Reset", command=reset_measurement)
start_button.grid(row=1, column=1, padx=3, pady=3, sticky=tk.W) 
stop_button.grid(row=1, column=2, padx=3, pady=3, sticky=tk.W)
pause_button.grid(row=1, column=3, padx=3, pady=3, sticky=tk.W)

# output frame   name-value.csv
#
output_file_save_path = tk.StringVar()                                          # path to saved file
output_file_save_name = tk.StringVar()						# output file name - auto save
output_file_save_number =tk.StringVar()						#                  - number
output_file_save_number.set("0001")

run_output_frame = ttk.LabelFrame(run_frame, text="Output File", relief=tk.RIDGE, padding=6)
run_output_frame.grid(row=1, column=2, padx=6, sticky=tk.E + tk.W + tk.N + tk.S)

output_file_head_entry = ttk.Entry(run_output_frame, textvariable=output_file_save_name, width=20)	# file name
output_file_head_entry.grid(row=1, column=1, sticky=tk.E + tk.N, pady=3)

output_file_dash_label = ttk.Label(run_output_frame, text="-")						# -
output_file_dash_label.grid(row=1, column=2, sticky=tk.E + tk.N)

output_file_tail_entry = ttk.Entry(run_output_frame, textvariable=output_file_save_number, width=20) 	# additional i.e file number
output_file_tail_entry.grid(row=1, column=3, sticky=tk.E + tk.N, pady=3)

output_file_ext_label = ttk.Label(run_output_frame, text=".xlsx")					# extension
output_file_ext_label.grid(row=1, column=4, sticky=tk.E + tk.N)

output_file_save_state =IntVar()
output_file_save_state.set(0)
output_file_save_butt = ttk.Checkbutton(run_output_frame, variable=output_file_save_state, onvalue=1, offvalue=0, text="Auto Save")
output_file_save_butt.grid(row=2, column=1)

output_file_inc_state =IntVar()
output_file_inc_state.set(0)
output_file_inc_butt = ttk.Checkbutton(run_output_frame, variable=output_file_inc_state, text="Auto Inc")
output_file_inc_butt.grid(row=2, column=3)


# test flow control
#
flow_frame = ttk.LabelFrame(root, text="Flow", relief=tk.RIDGE, padding=6)
flow_frame.grid(row=3, column=2, padx=6, sticky=tk.E + tk.W + tk.N + tk.S)

flow_track_state =IntVar()
flow_track_state.set(1)
flow_track_butt = ttk.Checkbutton(flow_frame, variable=flow_track_state, onvalue=1, offvalue=0, text="Auto Track")
flow_track_butt.grid(row=1, column=1)

flow_stop_state =IntVar()
flow_stop_state.set(0)
flow_stop_butt = ttk.Checkbutton(flow_frame, variable=flow_stop_state, onvalue=1, offvalue=0, text="Stop on Error")
flow_stop_butt.grid(row=1, column=2)

flow_delay_var=tk.IntVar()
flow_delay_var.set(800)			                                        # default delay
flow_delay_label = ttk.Label(flow_frame, text="Delay ms")
flow_delay_label.grid(row=2, column=1, sticky=tk.W + tk.N)
flow_delay_entry = ttk.Entry(flow_frame, textvariable=flow_delay_var, width=10) # , font=lcdFontSmall)
flow_delay_entry.grid(row=2, column=2, sticky=tk.W, pady=3)

# top line menu

demo_state =IntVar()					# demo mode does not need USB devices
demo_state.set(0)

menubar = tk.Menu(root)

filemenu = tk.Menu(menubar, tearoff=0)
filemenu.add_command(label="Open Input XLSX", command=OpenInput)
filemenu.add_command(label="Save Output XLSX", command=SaveOutput)
filemenu.add_command(label="View Output XLXS", command=ViewOutput)
filemenu.add_command(label="Save Input XLXS", command=SaveInput)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

configmenu = Menu(menubar, tearoff=0)
configmenu.add_command(label="Clear Test", command=clear_test)
configmenu.add_command(label="Connect SG", command=signal_connect)
configmenu.add_command(label="Connect SA", command=analyzer_connect)
configmenu.add_command(label="Run Config", command=run_config)
configmenu.add_command(label="Demo", command=demoMode)
menubar.add_cascade(label="Config", menu=configmenu)

helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="Help", command=donothing)
helpmenu.add_command(label="About...", command=donothing)
menubar.add_cascade(label="Help", menu=helpmenu)

root.config(menu=menubar)

# Run the GUI main loop
root.mainloop()

