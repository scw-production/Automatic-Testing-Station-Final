# -*- coding: utf-8 -*-

# This example generates a basic CW signal.

from sg_api import *
from time import sleep

def output_cw():
    # Open device
    device = sg_open_device()["handle"]
    sg_close_device(device)
    # Configure the device to output a -20 dBm CW signal at 1GHz
    sg_set_frequency_amplitude(device, 155000000, -20.0)
    sg_set_cw(device)

    # Will transmit until you close the device or abort
    sleep(2)

    # Done with device
    

if __name__ == "__main__":
    output_cw()
